package kvstore

import akka.actor.*
import akka.actor.SupervisorStrategy.Stop
import kvstore.Arbiter.*
import akka.pattern.{ask, pipe}

import scala.concurrent.duration.*
import akka.util.Timeout

object Replica:
  sealed trait Operation:
    def key: String
    def id: Long
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  case class Save(key: String, value: Option[String], id: Long, requester: ActorRef)

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(Replica(arbiter, persistenceProps))

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor:
  import Replica.*
  import Replicator.*
  import Persistence.*
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var persistence = context.actorOf(persistenceProps)
  var runningPersistence = Map.empty[Long, (ActorRef, Cancellable)]

  // Store the acks from the replicators
  var replicatorAcksReceived = Map.empty[ActorRef, Boolean]

  var expectedId: Long = 0L

  private def enoughAckReceived(): Boolean =
    if replicatorAcksReceived.isEmpty then true
    else replicatorAcksReceived.values.count(_ == true) > replicatorAcksReceived.size / 2

  private def operationCompleted(id: Long): Boolean =
    enoughAckReceived() && runningPersistence(id)._2.isCancelled

  override def preStart() =
    arbiter ! Join

  override val supervisorStrategy = OneForOneStrategy() {
    case _: PersistenceException =>
      println("Perstence Crashed!")
      akka.actor.SupervisorStrategy.Restart
  }

  def receive =
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)

  /* TODO Behavior for the leader role. */
  val leader: Receive =
    case Replicas(replicas: Set[ActorRef]) =>
      var newSecondaries = secondaries
      secondaries.filter((r, _) => !secondaries.contains(r)).foreach(
        (replica, replicator) =>
          replicators = replicators - replicator
          replicatorAcksReceived =  replicatorAcksReceived.removed(replicator)
          replicator ! Stop
          newSecondaries = newSecondaries.removed(replica)
      )

      replicas.filter((r: ActorRef) => r != self && !secondaries.contains(r)).foreach(
        (r: ActorRef) =>
          val replicator = context.actorOf(Replicator.props(r))
          replicators = replicators + replicator
          secondaries = secondaries.updated(r, replicator)
          kv.foreach((k, v) => replicator ! Replicate(k, Some(v), 0))
      )


    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)

    case Insert(key, value, id) =>
      kv = kv.updated(key, value)
      self ! Save(key, Some(value), id, sender)

    case Remove(key, id) =>
      kv = kv.removed(key)
      self ! Save(key, None, id, sender)

    case Save(key, valueOption, id, requester) =>
      context.setReceiveTimeout(1.second)
      context.become(waitForPersisted(requester, id))
      val cancellable = context.system.scheduler.scheduleWithFixedDelay(
        0.milliseconds, 100.milliseconds, persistence, Persist(key, valueOption, id))
      runningPersistence = runningPersistence.updated(id, (self, cancellable))
      replicators.foreach((repl: ActorRef) =>
        repl ! Replicate(key, valueOption, id)
          replicatorAcksReceived = replicatorAcksReceived.updated(repl, false)
      )

    case ReceiveTimeout =>

  // Adapted from https://stackoverflow.com/questions/22770927/waiting-for-multiple-results-in-akka
  def waitForPersisted(client: ActorRef, operationId: Long): Receive =
    case ReceiveTimeout =>
      client ! OperationFailed(operationId)
      context.become(leader)
    case Replicated(key, id) =>
      replicatorAcksReceived = replicatorAcksReceived.updated(sender, true)
      if operationCompleted(id) then
        client ! OperationAck(id)
        context.become(leader)
    case Persisted(key, id) =>
      runningPersistence(id)._2.cancel()
      if operationCompleted(id) then
        client ! OperationAck(id)
        context.become(leader)


  /* TODO Behavior for the replica role. */
  val replica: Receive =
    case Snapshot(key, valueOption: Option[String], id) =>
      if id < expectedId then sender ! SnapshotAck(key, id)
      else if id == expectedId then
        valueOption match
          case Some(value) => kv = kv.updated(key, value)
          case None => kv = kv.removed(key)
        expectedId += 1
        val cancellable = context.system.scheduler.scheduleWithFixedDelay(0.milliseconds, 100.milliseconds,
          persistence, Persist(key, valueOption, id))
        runningPersistence = runningPersistence.updated(id, (sender, cancellable))

    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)

    case Persisted(key, id) =>
      runningPersistence(id)._2.cancel()
      val originalSender = runningPersistence(id)._1
      originalSender ! SnapshotAck(key, id)
