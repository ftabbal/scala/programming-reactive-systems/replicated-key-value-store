package kvstore

import akka.actor.Props
import akka.actor.Actor
import akka.actor.Scheduler
import akka.actor.Cancellable
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import kvstore.Replicator.SnapshotAck

import scala.concurrent.duration.*

object Replicator:
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(Replicator(replica))

class Replicator(val replica: ActorRef) extends Actor:
  import Replicator.*
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  var runningSnapshots = Map.empty[Long, (Long, Cancellable)]

//  context.setReceiveTimeout(100.milliseconds)

  var _seqCounter = 0L
  def nextSeq() =
    val ret = _seqCounter
    _seqCounter += 1
    ret

  
  /* TODO Behavior for the Replicator. */
  def receive: Receive =
    case Replicate(key, valueOption, id) =>
      acks = acks.updated(id, (sender, Replicate(key, valueOption, id)))
      val seqNum = nextSeq()
      val cancellable = context.system.scheduler.scheduleWithFixedDelay(0.milliseconds, 100.milliseconds,
        replica, Snapshot(key, valueOption, seqNum))
      runningSnapshots = runningSnapshots.updated(seqNum, (id, cancellable))
    case SnapshotAck(key, seqNum) =>
      runningSnapshots.get(seqNum) match
        case Some(id: Long, task: Cancellable) =>
          task.cancel()
          val (s, message) = acks(id)
          s ! Replicated(key, id)
          acks = acks.removed(id)
          runningSnapshots = runningSnapshots.removed(seqNum)
        case None =>
